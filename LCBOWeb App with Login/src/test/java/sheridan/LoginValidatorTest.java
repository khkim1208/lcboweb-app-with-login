package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {
	
	// Test for requirement 1
	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses123" ) );
	}
	
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "!@#$%^" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "r1amses" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "123Ramses" ) );
	}
	
	
	// Test for requirement 2
	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses123" ) );
	}
	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "!@#$%^" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ram123" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ram12" ) );
	}
	



}
