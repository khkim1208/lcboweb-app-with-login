package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		if(Character.isDigit(loginName.charAt(0))) {
			return false;
		}		
		
		return loginName.length( ) >= 6 && loginName.matches("^[a-zA-Z0-9]*$");
	}
}
